// console.log("Hello World!")

// Old way of storing data using variable/s

let studentNumberA = "2023-1923";
let studentNumberB = "2023-1924";
let studentNumberC = "2023-1925";
let studentNumberD = "2023-1926";
let studentNumberE = "2023-1927";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);

// Using array/s we can store multiple data with similar category or data types.
let studentNumbers = ["2023-1923", "2023-1924", "2023-1925", "2023-1926", "2023-1927"];

console.log(studentNumbers);


// [SECTION] Arrays
	/*
		- Arryas are used to store multiple related values in a single variable.
		- They are declared using square brackets ([]), also know as "Array Literals".

		Syntax:
			let/const arrayName = [elementA, elementB, elementC, .... elementN];

	*/


// Common examples of arrays:

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsi", "Apple"];


// This is possible but not recommended. Better use object literals instead of array literals in storing mixed data.properties.
let mixedArr = ["Asus", 2020, "China", null];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);


// Alternative ways to write arrays

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
];

console.log(myTasks);

// Creating an array with values from variable.
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";


let cities = [city1, city2, city3];

console.log(cities);


// [SECTION] Length Property

	// The .length property allows us to get and set the total number of items/elemens in an array.

console.log(myTasks.length);
console.log(cities.length);


let blankArr = [];
console.log(blankArr.length);


// To get the lenght of a String.
let fullName = "Adam Sandler";
console.log(fullName.length);

// .length property on strings shows the number of characters in the string. Spaces are counted as characters in string data type/ strings.



// The length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array.
myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

// deleting the last element using decrement
cities.length--;
console.log(cities);


// This approach does not work with strings
fullName.length = fullName.length -1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);



// Adding elements using the .length property.

let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles);
// theBeatles.length++; 	
theBeatles.length++
theBeatles[4] = "Rich";
console.log(theBeatles);

// [SECTION] Reading from Arrays

	/*
		- Accessing array elements is one of the more commmon tasks that we do with arrays.
		- This can be done through the use of array indexes.
			- aeach element in an array has an index assigned to it.

		- memory address of array 
			Array address 0x7ffe9472bad0
				Array[0]: 0x7ffe96912ct0f

		Syntax:
			arrayName[index];
	*/

console.log(grades[0]);
console.log(computerBrands[3]);

// Accessing an array element that does not exist will return "undefined".
console.log(grades[5]);

let lakersLegends = ["Kobe", "Shaq", "leBron", "Magic", "Kareem"];
// Accessing the second element in the array.
console.log(lakersLegends[1]);
// Acceessing the fourth element in the array
console.log(lakersLegends[3]);

// We can get the last element using .length property.
console.log(lakersLegends.length);
console.log(lakersLegends[lakersLegends.length -1]);


// We can also save array element's in another variable
let currentLakers = lakersLegends[2];
console.log(currentLakers);

// You can also reassign (update) array values using the index of the element.
console.log("Array before reassingment:");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Result after reassingment:");
console.log(lakersLegends);


// Accessing the last element of the array.
let bullsLegends = ["Rodman", "Jordan", "Pippen", "Rose", "Kukoc"];

let lastElement = bullsLegends.length -1;

console.log(bullsLegends[lastElement]);

// much direct approach
console.log(bullsLegends[bullsLegends.length -1]);
// This accesses the second to the last element
console.log(bullsLegends[bullsLegends.length -2]);




// Adding items into an array

let newArr = [];
console.log(newArr[0]);


newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);


// This is possible but not recommended.
// newArr[5] = "Super Mario";
// console.log(newArr);	

// Adding element at the end using .length property.
newArr[newArr.length] = "Felix";
console.log(newArr);
newArr[newArr.length] = "Super Mario";
console.log(newArr);
newArr[newArr.length] = "Hoshi";
console.log(newArr);


// Traversing an array using loops

	for(let index = 0; index < newArr.length; index++) {
		console.log(newArr[index]);
	};



// Travesing an array of numbers using loops 
	
	let numArr = [5, 12, 30, 46, 40];

	for(let index = 0; index < numArr.length; index++) {
		if (numArr[index] % 5 === 0) {
			console.log(numArr[index] + " is divisible by 5");
		}
		else {
			console.log(numArr[index] + " is not divisible by 5");
		}
	};

// [SECTION] Multi-dimensional Arrays
	
	let chessBoard = [
		["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
		["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
		["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
		["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
		["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
		["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
		["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
		["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
	];



console.log(chessBoard);

// Output e7 in the console.
console.log(chessBoard[6][4]);

console.log(chessBoard[2][6]);


console.log("The king moves to : " + chessBoard[1][5]);


